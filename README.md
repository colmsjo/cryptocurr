Overview
=========


Markets
-------

Bitcoin:
 * btcx.sd - har konto
 * MtGox - submitted request for verification - https://www.mtgox.com/fee-schedule
 * Bitstamp - submitted request for verification - https://www.bitstamp.net/fee_schedule/ , 0,9 EUR withdrawal fee
 * bitcoin-central.net - måsta skicka in ID och pass
 * Kapiton - ingen verifiering?
 * bitcoin.cz
 * Coinbase - US only
 * campbx

Flera currencies:
 * vircurex - 17 crypto currencies, har API
 * btc-e - BTC, LTC, Namecoin, API
 * crypto.st - BTC, LTC, FTC, långsam site, saknar API?
 * coinmkt.com - trade several crypto currencies, verified, inget API, 0,5% fee, 0.001 withdrawal fee, 
 * therocktrading - BTC, LTC, Ripple (1%) trading fee
 * cryptsy.com - 60 cryptocurrencies

Commisions:s
 * Mt. Gox: 0.6%
 * Bitstamp: 0.5%
 * Intersango: 0.65%
 * Bitcoin-Central: 0.498%
 * btc-e: 0,2%


Trading platform:

  * Kraken.com - Bitcoin, Litecoin, Namecoin, Ripple, Ven 


Översikt

|       Marknad      | Currencies            | Trading fee  | Withdrawal fee             | Withdrawal limit | API |
| ------------------ |:---------------------:| ------------:|---------------------------:|-----------------:|-----|
| btc-e.com          | BTC, LTC, Namecoin    |         0,2% | 0.002 BTC                  | 100 BTC          | X   |
| vircurex.com       | 17 crypto currencies  |         0,2% | 0.001 BTC                  | no limit         | X   |
| therocktrading.com | BTC, LTC, Ripple      |           1% |                            |                  |     |
| cryptsy.com        | 60 cryptocurrencies   |     0,2-0,3% | 0.0005 BTC                 |                  | X   |



Information
-----------

Links:

 * http://bitcoinity.org/
 * http://bitcoin-analytics.com/
 * https://cointhink.com/
 * http://trading.i286.org/
 * http://www.weusecoins.com/en/
 * https://bitpay.com/directory#/


Market cap per currency - http://coinmarketcap.com/
Diddiculty to mine - http://www.coinwarz.com/cryptocurrency

Including non-minable:
 1. Bitcoin
 1. Ripples
 1. Litecoin
 1. Peercoin
 1. MasterCoin
 1. Nxt
 1. Namecoin
 1. Quark
 1. ProtoShares
 1. Megacoin

Excluding non-minable:

 1. Bitcoin
 1. Litecoin
 1. Peercoin
 1. Namecoin
 1. Quark
 1. ProtoShares
 1. Megacoin
 1. WorldCoin
 1. PrimeCoin
 1. FeatherCoin


Open source trading software
----------------------------

https://github.com/maxme/bitcoin-arbitrage



Kolla upp
---------

Ripple.com




API Examples
============


Full orderbook:

 * http://pubapi.cryptsy.com/api.php?method=orderdata


Orderbook for Namecoins (NMC) in Bitcon (BTC):

 * https://vircurex.com/api/orderbook.json?base=BTC&alt=NMC
 * https://vircurex.com/api/orderbook.json?base=NMC&alt=BTC
 * http://pubapi.cryptsy.com/api.php?method=singleorderdata&marketid=29 




Arbitrage
========

Namecoin: ~$5
Bitcoin: ~700

Vircurex orderbook for NMC/BTC:

```
{"asks":[
["0.00717472","65.0"],
["0.00717473","68.44388"],
["0.00717497","3.7"],
["0.00717499","0.04"],
...
"bids":[
["0.00701062","50.0"],
["0.00701061","807.18224"],
["0.00701055","14.26423642"],

```


Cryptsy orderbook for NMC in BTC:

```
"success":1,"return":{"NMC":{"marketid":"29","label":"NMC\/BTC","primaryname":"NameCoin","primarycode":"NMC","secondaryname":"BitCoin","secondarycode":"BTC",
"sellorders":
[{"price":"0.00709911","quantity":"15.80253141","total":"0.11218391"},
{"price":"0.00709919","quantity":"4.21736971","total":"0.02993991"},
{"price":"0.00709921","quantity":"0.61046177","total":"0.00433380"},
{"price":"0.00709922","quantity":"5.51694264","total":"0.17194594"},
{"price":"0.00709924","quantity":"24.17585107","total":"0.26977112"},
{"price":"0.00715836","quantity":"0.02072725","total":"0.00014837"},
{"price":"0.00717462","quantity":"56.33556854","total":"0.40418629"},
{"price":"0.00717478","quantity":"3.34764530","total":"0.02401862"},
...
"buyorders":
[{"price":"0.00697948","quantity":"0.71695944","total":"0.00500400"},
{"price":"0.00697947","quantity":"42.89792803","total":"0.29940480"},
{"price":"0.00697946","quantity":"1.61071076","total":"0.01124189"},
{"price":"0.00697940","quantity":"3.76566456","total":"0.02628208"},
{"price":"0.00697789","quantity":"100.00000000","total":"0.69778900"},
{"price":"0.00697773","quantity":"106.96183747","total":"0.74635082"},
```
















