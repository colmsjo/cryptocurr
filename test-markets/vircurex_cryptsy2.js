#!/usr/bin/env node

//
// -------------------
// API: https://vircurex.com/welcome/api
// Using lib: https://github.com/petermrg/node-vircurex
//
// curl -i "https://vircurex.com/api/orderbook.json?base=BTC&alt=NMC"
// {"asks":[
// ["0.00717472","65.0"],
// ...
// "bids":[
// ["0.00701062","50.0"],
// -------------------
// API: https://www.cryptsy.com/pages/api
// Using lib: https://github.com/petermrg/node-cryptsy
//
// curl -i "http://pubapi.cryptsy.com/api.php?method=singleorderdata&marketid=29"
//
// "success":1,"return":{"NMC":{"marketid":"29","label":"NMC\/BTC","primaryname":"NameCoin","primarycode":"NMC","secondaryname":"BitCoin","secondarycode":"BTC",
// "sellorders":
// [{"price":"0.00709911","quantity":"15.80253141","total":"0.11218391"},
// ...
// "buyorders":
// [{"price":"0.00697948","quantity":"0.71695944","total":"0.00500400"},
// -------------------
//

var VERBOSE = false,
	HEARTBEAT = true;

var stream     = process.stdout,
	logStream  = null; // can use process.stderr

var Vircurex = require('vircurex')
var vircurex = new Vircurex('colmsjo', {'getBalance': 'mysecretcode'})
var Cryptsy = require('cryptsy');
var cryptsy = new Cryptsy('8e421da6b169681f33b301e6076c96458c68e2bc', 
  '2af0d0562757c7a83e65c4a6925a85e36491eb31b7e60961429ff012cf9f08ae75cfd8fe56ed9363');

//For todays date;
Date.prototype.today = function(){ 
    return this.getFullYear()+"-"+
    (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1)+"-"+
    ((this.getDate() < 10)?"0":"") + this.getDate() 
};

//For the time now
Date.prototype.timeNow = function(){
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
};

function date() {
	var date = new Date();
	return date.today() + " " + date.timeNow();
}

function writeMessage(message) {
	if(message.message_type != "info" || VERBOSE) stream.write( JSON.stringify(message) );
}

function logErr(err) {
	if(logStream)
		logStream.write(JSON.stringify({
			time: date(),
			message_type: "error",
			error_message: ""+err
		}));
}

function sortCryptsy(a,b,order) {
	return order*(a.price - b.price);
}

function sortCryptsyAsc(a,b)  { return sortCryptsy(a,b,1); }
function sortCryptsyDesc(a,b) { return sortCryptsy(a,b,-1);}

this.cryptsy = {};
this.cryptsy.sellorders = null;
this.cryptsy.buyorders  = null;

function updateCryptsyOrderbook() {
	cryptsy.api('singleorderdata', { marketid: 29 }, function (err, data) {
	    if (err) {
	        logErr(err);
	    } else {
	    	this.cryptsy.sellorders = data.NMC.sellorders;
	    	this.cryptsy.sellorders.sort(sortCryptsyAsc);

	    	this.cryptsy.buyorders  = data.NMC.buyorders;
	    	this.cryptsy.buyorders.sort(sortCryptsyAsc);
	    };
	}.bind(this));
}

function sortVircurex(a,b,order) {
	return order*(a[0] - b[0]);
}

function sortVircurexAsc(a,b)  { return sortVircurex(a,b,1); }
function sortVircurexDesc(a,b) { return sortVircurex(a,b,-1);}

this.vircurex = {};
this.vircurex.sellorders = null;
this.vircurex.buyorders  = null;

function updateVircurexOrderbook() {
	vircurex.getOrders('NMC', 'BTC', function(err, data) {
	  if (!err) {
	    this.vircurex.sellorders = data.asks;
	    this.vircurex.sellorders.sort(sortVircurexAsc);

	    this.vircurex.buyorders = data.bids;
	    this.vircurex.buyorders.sort(sortVircurexAsc);
	  }
	  else logErr(err);
	}.bind(this));
}

function checkOpportunities() {

	if(!this.cryptsy.sellorders ||
	   !this.cryptsy.buyorders ||
	   !this.vircurex.sellorders||
	   !this.vircurex.buyorders) return;

	// Print heartbeat once per minute
	if(HEARTBEAT && counter++ % 20 == 0) 
		writeMessage({
			time: date(), 
			message_type: "counter",
			counter:counter}
		);

    var message = {};

	if(this.vircurex.sellorders[0][0] < this.cryptsy.buyorders[0].price) {
		message = {
					time: date(),
					message_type: "trade",
    				opportunity: true,
			        sell_market: "vircurex", sell_json: this.vircurex.sellorders[0],
				    buy_market:  "cryptsy",  buy_json:  this.cryptsy.buyorders[0]
				};
	}

	if(this.cryptsy.sellorders[0].price < this.vircurex.buyorders[0][0]) {
		message = {
					time: date(),
					message_type: "trade",
    				opportunity: true,
			        sell_market: "cryptsy",  sell_json: this.cryptsy.sellorders[0],
				    buy_market:  "vircurex", buy_json:  this.vircurex.buyorders[0]
				};
	}

	message = {
				time: date(),
				message_type: "info",
				opportunity: false,
		        market1: "cryptsy",  sell_json1: this.cryptsy.sellorders[0],  buy_json1: this.cryptsy.buyorders[0],
			    market2: "vircurex", sell_json2: this.vircurex.sellorders[0], buy_json2: this.vircurex.buyorders[0]
			};

	writeMessage(message);
}


var counter = 0;
var interval = 3000;
setInterval(updateCryptsyOrderbook.bind(this),  interval);
setInterval(updateVircurexOrderbook.bind(this), interval);
setInterval(checkOpportunities.bind(this),      interval);

