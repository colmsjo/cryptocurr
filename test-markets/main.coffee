#!/usr/bin/env coffee



# Main
# ======================================================================


_interval = 3000
arbiter = require('./vircurex_cryptsy.coffee')
_arbiter  = arbiter.create(false, true)

setInterval( 
	() =>
		_arbiter.updateCryptsyOrderbook()
	
	_interval)


setInterval( 
	() =>
		_arbiter.updateVircurexOrderbook()
	
	_interval)

setInterval( 
	() =>
		_arbiter.checkOpportunities()
	
	_interval)
