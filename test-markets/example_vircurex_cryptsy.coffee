exports = exports ? this

exports.create = () ->

	sortVircurex : (a,b,order) ->
		return (order ? 1)*(a[0] - b[0])

	sortVircurexAsc : (a,b) ->
		return this.sortVircurex(a,b,1)

	sortVircurexDesc : (a,b) ->
		return -1*(a[0] - b[0])

	_vircurex : {}

	_vircurex_sellorders : null

	_vircurex_buyorders  : null

	updateVircurexOrderbook : ()  ->

		`
		var Vircurex = require('vircurex')
		_vircurex = new Vircurex('colmsjo', {'getBalance': 'mysecretcode'})
		`

		_vircurex.getOrders('NMC', 'BTC', 
			(err, data) =>
				if (!err)
					this._vircurex_sellorders = data.asks
					this._vircurex_sellorders.sort(this.sortVircurex)

					this._vircurex_buyorders = data.bids
					this._vircurex_buyorders.sort(this.sortVircurexDesc)

					console.log("Vircurex1 sell:"+JSON.stringify(this._vircurex_sellorders))
					console.log("Vircurex1 buy:"+JSON.stringify(this._vircurex_buyorders))
				else 
					console.log("vircurex2:"+JSON.stringify(err))
		)

	sortCryptsy : (a,b,order) ->
		return (order ?  1)*(a.price - b.price)

	sortCryptsyDesc : (a,b) ->
		return -1*(a.price - b.price)

	_cryptsy            : {}

	_cryptsy_sellorders : null

	_cryptsy_buyorders  : null

	updateCryptsyOrderbook : () ->

		`
		var Cryptsy = require('cryptsy');
		_cryptsy = new Cryptsy('8e421da6b169681f33b301e6076c96458c68e2bc', 
		  '2af0d0562757c7a83e65c4a6925a85e36491eb31b7e60961429ff012cf9f08ae75cfd8fe56ed9363');
		`

		_cryptsy.api('singleorderdata', { marketid: 29 }, 
			(err, data) =>
				if (err)
					this.logErr("cryptsy:"+err)
				else
					this._cryptsy_sellorders = data.NMC.sellorders
					this._cryptsy_sellorders.sort(this.sortCryptsy)

					this._cryptsy_buyorders  = data.NMC.buyorders
					this._cryptsy_buyorders.sort(this.sortCryptsyDesc)
					console.log("Cryptsy sell:"+JSON.stringify(this._cryptsy_sellorders))
					console.log("Cryptsy buy:"+JSON.stringify(this._cryptsy_buyorders))
		)


