exports = exports ? this

#
# 2014-01-01 Jonas Colmsjö
#
# vircurex_cryptsy - look for arbitrage opportunities between Vircurex and Cruptsy
#


exports.create = (verbose, hearbeat) ->

	# Config
	# ======================================================================

	VERBOSE    : verbose ? true 

	HEARTBEAT  : hearbeat ? true

	_stream    : process.stdout

	# _logStream : process.stderr

	_counter   : 0


	# helpers
	# ======================================================================

	mydate : () ->
		_date = Date()
		return Date()

	# Functions
	# ======================================================================

	writeMessage : (message) ->
		if(message.message_type != "info" || this.VERBOSE) 
			this._stream.write( JSON.stringify(message) )

	logErr : (err) ->
		if(this._logStream?)
			this._logStream.write(JSON.stringify({
				time:         this.mydate(),
				message_type: "error",
				error_message: err
			}));


	# Cryptsy
	# ----------------------------------------------------------------------

	sortCryptsy : (a,b,order) ->
		return (order ?  1)*(a.price - b.price)

	sortCryptsyDesc : (a,b) ->
		return -1*(a.price - b.price)

	_cryptsy            : {}

	_cryptsy_sellorders : null

	_cryptsy_buyorders  : null

	updateCryptsyOrderbook : () ->

		`
		var Cryptsy = require('cryptsy');
		_cryptsy = new Cryptsy('8e421da6b169681f33b301e6076c96458c68e2bc', 
		  '2af0d0562757c7a83e65c4a6925a85e36491eb31b7e60961429ff012cf9f08ae75cfd8fe56ed9363');
		`

		_cryptsy.api('singleorderdata', { marketid: 29 }, 
			(err, data) =>
				if (err)
					this.logErr("cryptsy:"+err)
				else
					this._cryptsy_sellorders = data.NMC.sellorders
					this._cryptsy_sellorders.sort(this.sortCryptsy)

					this._cryptsy_buyorders  = data.NMC.buyorders
					this._cryptsy_buyorders.sort(this.sortCryptsyDesc)
		)


	# Vircurex
	# ----------------------------------------------------------------------

	sortVircurex : (a,b,order) ->
		return (order ? 1)*(a[0] - b[0])

	sortVircurexDesc : (a,b) ->
		return -1*(a[0] - b[0])

	_vircurex : {}

	_vircurex_sellorders : null

	_vircurex_buyorders  : null

	updateVircurexOrderbook : ()  ->

		`
		var Vircurex = require('vircurex')
		_vircurex = new Vircurex('colmsjo', {'getBalance': 'mysecretcode'})
		`

		_vircurex.getOrders('NMC', 'BTC', 
			(err, data) =>
				if (!err)
					this._vircurex_sellorders = data.asks
					this._vircurex_sellorders.sort(this.sortVircurex)

					this._vircurex_buyorders = data.bids
					this._vircurex_buyorders.sort(this.sortVircurexDesc)
				else 
					this.logErr("vircurex:"+JSON.stringify(err))
		)


	# Arbitrage
	# ----------------------------------------------------------------------

	checkOpportunities : () ->

		if(!this._cryptsy_sellorders || !this._cryptsy_buyorders || !this._vircurex_sellorders|| !this._vircurex_buyorders)
			return

		# Print heartbeat once per minute
		if(this.HEARTBEAT && this._counter++ % 20 == 0) 
			this.writeMessage({
				time:         this.mydate(), 
				message_type: "counter",
				counter:      this._counter}
			)

		_message = {}

		if(this._vircurex_sellorders[0][0] < this._cryptsy_buyorders[0].price)
			_message = {
						time:         this.mydate(),
						message_type: "trade",
						opportunity:  true,
						sell_market:  "vircurex", sell_json: this._vircurex_sellorders[0],
						buy_market:   "cryptsy",  buy_json:  this._cryptsy_buyorders[0]
					}

		if(this._cryptsy_sellorders[0].price < this._vircurex_buyorders[0][0])
			_message = {
						time:         this.mydate(),
						message_type: "trade",
						opportunity:  true,
						sell_market:  "cryptsy",  sell_json: this._cryptsy_sellorders[0],
						buy_market:   "vircurex", buy_json:  this._vircurex_buyorders[0]
					}


		_message = {
					time: this.mydate(),
					message_type: "info",
					opportunity: false,
					market1: "cryptsy",  sell_json1: this._cryptsy_sellorders[0],  buy_json1: this._cryptsy_buyorders[0],
					market2: "vircurex", sell_json2: this._vircurex_sellorders[0], buy_json2: this._vircurex_buyorders[0]
				}

		this.writeMessage(_message)

