#!/usr/bin/env node

//
// Using lib: https://github.com/petermrg/node-cryptsy
//
// API: https://www.cryptsy.com/pages/api
//
//

var Cryptsy = require('cryptsy');
var cryptsy = new Cryptsy('8e421da6b169681f33b301e6076c96458c68e2bc', 
	'2af0d0562757c7a83e65c4a6925a85e36491eb31b7e60961429ff012cf9f08ae75cfd8fe56ed9363');

// curl -i "http://pubapi.cryptsy.com/api.php?method=singleorderdata&marketid=29"
// curl -i "http://pubapi.cryptsy.com/api.php?method=orderdata&marketid=29"

cryptsy.api('singleorderdata', { marketid: 29 }, function (err, data) {
    if (err) {
        throw err;
    } else {
    	console.log(JSON.stringify(data))
    }
});
