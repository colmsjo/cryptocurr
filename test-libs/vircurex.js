#!/usr/bin/env node


var Vircurex = require('vircurex')
var vircurex = new Vircurex('colmsjo', {
  'getBalance': 'mysecretcode',
})

vircurex.getBalance('usd', function(err, data) {
  if (!err) console.log(data)
  else console.log(err)
})

/*
Console output:

{ account: 'JohnDoe',
  currency: 'usd',
  balance: '1234.0',
  availablebalance: '1234.0',
  timestamp: '2013-05-21T09:11:12+00:00',
  token: 'e489c434c5f5b01b3eba9d58eed98bcc4d2836f6c484cfebdf8e234bc4931c2a',
  function: 'get_balance',
  status: 0 }
*/



vircurex.getOrders('BTC', 'NMC', function(err, data) {
  if (!err) console.log(data)
  else console.log(err)
})
