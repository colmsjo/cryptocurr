#!/usr/bin/env node


var fetch = require('cryptofetch');

// Query the current data for LTC in terms of LTC/BTC on BTC-e
fetch('btc-e', 'ltc', 'btc').then(function (data) {
  console.log(data);
  /*
  {
    high: 0.03889,
    low: 0.0301,
    avg: 0.034495,
    vol: 22371.85528,
    last: 0.03626,
    buy: 0.03626,
    sell: 0.03611
  }
  */

});

// Query the current data for LTC in terms of LTC/BTC on vircurex
fetch('vircurex', 'ltc', 'btc').then(function (data) {
  console.log(data);
  /*
  { high: 0.03171013, low: 0.03188, last: 0.032 }
  */

});